import React from "react";
import { Link } from "react-router-dom";

// Компонент для вывода списка товаров, аналогичен элементу в ProductsPage
const ProductList = ({ products, deleteProduct, getPrice }) => {
  return (
    <div className="py-4">
      <table className="table bg-light">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Страна</th>
            <th scope="col">Бренд</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {products &&
            products.map(product => (
              <tr key={product.id_product}>
                <th scope="row">{product.id_product}</th>
                <td>{product.type_product}</td>
                <td>{product.price_product}р.</td>
                <td>{product.country_product}</td>
                <td>{product.brand_product}</td>
                <td>
                  <div className="add-btn">
                    <i
                      className="fa fa-minus-circle text-primary add-btn-icon"
                      onClick={() => {
                        deleteProduct(product.id_product);
                        setTimeout(() => getPrice(), 300);
                      }}
                    />
                  </div>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};

// Компонент корзины, тут выводятся товары добавленные на страницу товаров
class CartPage extends React.Component {
  state = {
    totalPrice: 0
  };

  getTotalPrice = () => {
    console.log("hello");
    let tempPrice = 0;
    this.props.products.map(product => (tempPrice += product.price_product));
    this.setState({ totalPrice: tempPrice });
  };

  componentDidMount() {
    this.getTotalPrice();
  }

  render() {
    const { totalPrice } = this.state;
    const { deleteProduct, products } = this.props;
    // Если товары в корзине есть, они выводятся, если нет, пишет Корзина пуста
    return (
      <div className="container">
        <h2 className="py-4">Выбранные вами товары</h2>
        {totalPrice !== 0 && <p>Сумма заказа: {totalPrice}р.</p>}
        {products.length ? (
          <ProductList
            products={products}
            deleteProduct={deleteProduct}
            getPrice={this.getTotalPrice}
          />
        ) : (
          <div>
            <h4>
              <i className="fa fa-shopping-cart" /> Корзина пуста
            </h4>
            <p>
              Вернуться к <Link to="/products">списку товаров</Link>?
            </p>
          </div>
        )}
      </div>
    );
  }
}

export default CartPage;
