import React from "react";
import { Link } from "react-router-dom";

// Компонент главной страницы, тут просто разметка
class IndexPage extends React.Component {
  state = {};

  render() {
    return (
      <div className="container">
        <div className="img-overlay">
          <div className="main-screen d-flex flex-column align-content-center justify-content-center">
            <div className="text-box">
              <h1 className="text-primary">Спортивный магазин!</h1>
              <a href="#description" className="Arrow">
                <i className="fa fa-arrow-circle-down" />
              </a>
            </div>
          </div>
        </div>

        <div className="description py-4" id="description">
          <div className="row">
            <div className="col-8 offset-2">
              <p className="Description">
                Откройте для себя спортивную одежду, обувь и спортивные товары
                брендов нашей группы в магазинах города Казань.Вы сможете найти
                все необходимое снаряжение для занятия Вашим любимым спортом.
                Профессиональные консультанты всегда рады встретить Вас и
                подобрать подходящую именно для Вас экипировку
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-8 offset-2">
            <div className="to-products py-4">
              <Link to="/products">К товарам</Link>
            </div>
          </div>
        </div>
        <div className="space" />
      </div>
    );
  }
}

export default IndexPage;
