import React from "react";
import axios from "axios";

// Компонент списка товаров, получается как аргументы массив обьектов товаров и функцию для добавления в карзину
const ProductList = ({ products = [], addProduct }) => {
  return (
    <div className="py-4">
      <table className="table bg-light">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Страна</th>
            <th scope="col">Бренд</th>
            <th />
          </tr>
        </thead>
        <tbody>
        {/*Если массив не пустой, в цикле 'map' Выводятся ряды с данными о товарах*/}
          {products &&
            products.map(product => (
              <tr key={product.id_product}>
                <th scope="row">{product.id_product}</th>
                <td>{product.type_product}</td>
                <td>{product.price_product}р.</td>
                <td>{product.country_product}</td>
                <td>{product.brand_product}</td>
                <td>
                  <div className="add-btn">
                    <i
                      className="fa fa-plus-circle text-primary add-btn-icon"
                      onClick={() => addProduct(product)}
                    />
                  </div>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};

class ProductsPage extends React.Component {
  // Состояние компонента, тут будут товары после скачивания
  state = {
    products: []
  };

  // Когда компонент помещен на страницу с сервера скачивается список товаров и помещается в локальное состояние: state (выше)
  componentDidMount() {
    axios
      .get("http://localhost:5000/api/product")
      .then(res => res.data)
      .then(res => this.setState({ products: res }));
  }

  render() {
    const { products } = this.state;
    const { addProduct } = this.props;
    // Если товары в а массиве есть, выводится список товаров, если нет, выводится слово 'Загрузка'
    return (
      <div className="container">
          <h3 className="py-4">Все товары</h3>
        {products.length ? (
          <ProductList products={products} addProduct={addProduct} />
        ) : (
          <h3>Загрузка...</h3>
        )}
      </div>
    );
  }
}

export default ProductsPage;
