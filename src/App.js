import React from "react";
import ProductsPage from "./pages/ProductsPage";
import CartPage from "./pages/CartPage";
import IndexPage from "./pages/IndexPage";
import { Route, Switch } from "react-router";
import Navbar from "./layouts/Navbar";

class App extends React.Component {
  state = {
    products: []
  };

  // Добавление товара, этот метод пробрасывается в компонент где отображается список всех продуктов
  // Вызывется при нажатии на знак плюс
  addProduct = product => {
    const tempProds = this.state.products.filter(
      p => p.id_product === product.id_product
    );
    if (!tempProds.length) {
      const products = this.state.products;
      products.push(product);
      this.setState({ products }, console.log(products));
    }
  };

  // Удаление товара из корзины, вызывается при нажатии на минус в корзине
  deleteProduct = id => {
    const products = this.state.products.filter(p => p.id_product !== id);
    this.setState({ products }, console.log(products));
  };

  // Отрисовка главного компонента
  render() {
    const { products } = this.state;
    return (
      <div className="App">
        {/*Навигация*/}
        <Navbar count={products.length} />
        <div className="Page">
          {/*Внутри этого компонента содержимое заменяется в зависимости от пути*/}
          <Switch>
            <Route path="/" exact component={IndexPage} />
            <Route
              // Например при пути 127.0.0.1:3000/products сюда будет помещен компонент ProductsPage
              path="/products"
              exact
              render={() => <ProductsPage addProduct={this.addProduct} />}
            />
            <Route
              path="/cart"
              exact
              render={() => (
                <CartPage
                  products={products}
                  deleteProduct={this.deleteProduct}
                />
              )}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
