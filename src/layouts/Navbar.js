import React from "react";
import { NavLink as Link } from "react-router-dom";

// Разметка панели навигации, сюда передается count - кол-во товаров в корзине
// link как a, ссылка, но не для перехода на другую страницу а для отрисовки другого компонента
const Navbar = ({count}) => (
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <div className="container">
      <Link className="navbar-brand text-primary" to="/">
        Shop`
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link activeClassName="active" className="nav-link" exact to="/">
              Главная
            </Link>
          </li>
          <li className="nav-item">
            <Link activeClassName="active" className="nav-link" to="/products">
              Список товаров
            </Link>
          </li>
        </ul>
        <ul className="navbar-nav float-right">
          <li className="nav-item">
            <Link className="nav-link bg-primary rounded" to="/cart">
              <i className="fa fa-shopping-cart" /> Выбранные товары <span className="badge badge-light text-dark">{count}</span>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  </nav>
);

export default Navbar;
